# Gwen - HTTP and TCP Service Monitoring

- HTTP Service
	- HTTP 80->443 Redirect
	- Valid TLS Certificate [WIP]
- TCP [WIP]
	- Accepting TCP connection [WIP]
- Alerts [WIP]
	- SMTP (Via CRON)
	- twillo [WIP]

# Compile

The normal golang stuffs.

	go build
	go install
	go run main.go

# Output

	true	http://example.com    [OK]
	true	https://example.com    [OK]
	true	https://federationtester.matrix.org/api/report?server_name=port22.co.uk    [Federation OK]
	Last Run:  2019-11-27 13:42:24.864238604 +0000 GMT m=+1.156346812

Generated HTML is placed in the current working directory as `status.html`

![screen shot of HTML output](screenshot.png)

# Usage

	Usage of gwen:
	  -config string
	    	A configuration file to load. (default "gwend.conf")
	  -output string
	    	HTML Output. (default "status.html")
	  -template string
	    	HTML Template. (default "templates/default-light.html")

# Deploy

	*/10 * * * * /usr/local/bin/gwen -config /etc/gwen/gwend.conf -output /www/public/status.html -template /etc/gwen/default-light.html > /dev/null

# License

Copyright 2019 Peter Maynard

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
