package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"html/template"
	"os"
	"strconv"
	"flag"
	"path/filepath"
	"strings"
	"net"
	"io"
)

// ================================================= //
// HTTP and Net
var TIMEOUT_VALUE = 30 * time.Second
// Monitor types
var MONI_HTTP_REDIRECT []string
var MON_DOMAINS []string
var MON_MATRIX []string
var MON_PORT []string
// Output
var MONITORED_RESPONSES MonitoredResponses
var RESPONSES []MonitoredResponse
// ================================================= //

// ================================================= //

type Config struct {
	Domain string `json:domain`
	Monitor []string `json:monitor`
}

type FedRes struct {
	FederationOK bool `json:FederationOK`
}
// ================================================= //

// Store the results of monitoring
type MonitoredResponses struct {
	Title string
	TimeStamp string
	Responses []MonitoredResponse
}

type MonitoredResponse struct {
	Status string
	Type string
	Domain string
	Message string
}
// ================================================= //

func HTTPGET(url string) (string, *http.Response) {
	c := &http.Client{Timeout: TIMEOUT_VALUE}
	// TODO: Deal with timeouts
	res, err := c.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}

	// Return the body and response
	return string(body), res
}

func checkHTTP(proto string, url string) {
	_, res := HTTPGET(proto+"://"+url)

	if res.StatusCode != http.StatusOK {
		result(proto, url, false, fmt.Sprintf("Response code was %v; want 200", res.StatusCode))
		return
	}

	if res.Header.Get("Content-Type") == "" {
		result(proto, url, false, "HTTP Header Content-Type was 'nil'; want anything else")
		return
	}

	// TODO: Check for HTTP->HTTPS redirect.
	result(proto, url, true, "OK")
}

// MON_MATRIX Stuff
func checkFedTester(url string) {
	body, _ := HTTPGET("https://federationtester.matrix.org/api/report?server_name="+url)
	var fedRes FedRes
	if err := json.Unmarshal([]byte(body), &fedRes); err != nil {
		fmt.Println("Error parsing JSON from federationtester.")
	}

	if fedRes.FederationOK {
		result("Matrix", url, true, "Federation OK")
	} else {
		result("Matrix", url, false, "Cannot Federate")
	}

}

// MON_PORT Stuff
func checkPort(domain string, proto string, port string){
	str := fmt.Sprintf("%s:%s", domain, port)
	one := make([]byte, 1)

	if proto == "tcp" {
		c, err := net.Dial(proto, fmt.Sprintf("%s:%s", domain, port))
		if err != nil {
			result(proto, str, false, "Cannot Initiate Connection")
			return 
		}

		c.SetReadDeadline(time.Now())
		_, err = c.Read(one)

		if err == io.EOF {
		  result(proto, str, false, "Detected closed connection")
		  c.Close()
		  c = nil
		  return
		}
		result(proto, str, true, "OK")
		return
	}

	if proto == "udp" { /*TODO*/ }

	result(proto, str, false, "Not supported")
}

func result(monitorType string, domain string, status bool, msg string) {
	result := fmt.Sprintf("%t\t%s    [%s]  \n", status, domain, msg)

	if status {
		fmt.Printf(result)
	}else{
		fmt.Fprintf(os.Stderr, result)
	}

	RESPONSES = append(RESPONSES, MonitoredResponse{strconv.FormatBool(status), strings.ToUpper(monitorType), domain, msg})
}

// Output
func createHTML(templateFile string, outputHTML string) {
	MONITORED_RESPONSES.Title = "Gwen - HTTP and TCP Service Monitoring"
	MONITORED_RESPONSES.TimeStamp = time.Now().Format("Nov 02 Mon 15:04")
	MONITORED_RESPONSES.Responses = RESPONSES

	// Load the template
	tmpl, err := template.New(filepath.Base(templateFile)).ParseFiles(templateFile)
	if err != nil {
		fmt.Println(err)
	}

	// Open file for writing 
	f, err := os.Create(outputHTML)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	// Fill in the template
	err = tmpl.Execute(f, MONITORED_RESPONSES)
	if err != nil {
		fmt.Println(err)
	}
}

func mianMonitor() {
	for _, d := range MON_DOMAINS {
		checkHTTP("https", d)
	}

	for _, d := range MONI_HTTP_REDIRECT {
		checkHTTP("http", d)
	}

	for _, d := range MON_MATRIX {
		checkFedTester(d)		
	}

	fmt.Println("Last Run: ", time.Now())
}

func loadConfig(file string) {
	// Open files
	fmt.Println("Config: ", file)
	data, err := ioutil.ReadFile(file)
    if err != nil {
      fmt.Println(err)
    }

    var conf []Config
	if err := json.Unmarshal(data, &conf); err != nil {
		fmt.Println("Error parsing JSON config file.")
	}

	for _, c := range conf {
		fmt.Println(c.Domain)
		for _, m := range c.Monitor {
			if !stringInSlice(c.Domain, MON_MATRIX) && m == "matrix" {
				MON_MATRIX = append(MON_MATRIX, c.Domain)
			}

			if !stringInSlice(c.Domain, MONI_HTTP_REDIRECT) && m == "http-redirect" {
				MONI_HTTP_REDIRECT = append(MONI_HTTP_REDIRECT, c.Domain)
			}

			if !stringInSlice(c.Domain, MON_DOMAINS) && m == "https" {
				MON_DOMAINS = append(MON_DOMAINS, c.Domain)
			}

			if !stringInSlice(c.Domain, MON_PORT) && strings.HasPrefix(m, "port-") {
				// MON_PORT = append(MON_PORT, c.Domain)
				m := strings.Split(m, "-")
				if(len(m) == 2) {
					checkPort(c.Domain, "tcp", m[1])
				}

				if(len(m) == 3) {
					checkPort(c.Domain, m[1], m[2])
				}
			}
		}
	}
}

func stringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func main() {
	// Flags

	var configFile string
	var templateFile string
	var outputHTML string
	flag.StringVar(&configFile, "config", "gwend.conf", "A configuration file to load.")
	flag.StringVar(&templateFile, "template", "templates/default-light.html", "HTML Template.")
	flag.StringVar(&outputHTML, "output", "status.html", "HTML Output.")
	flag.Parse()

	// Config
	loadConfig(configFile)

	// Monitor
	mianMonitor()

	// Output 
	createHTML(templateFile, outputHTML)
}
